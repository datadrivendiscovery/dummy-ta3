#!/bin/bash -e

python3 -m dummy_ta3.dummy_ta3 --problem-path "$D3MPROBLEMPATH" --datasets-dir "$D3MINPUTDIR" --time-bound "$D3MTIMEOUT" "$@"

find "$D3MOUTPUTDIR" -ls

# We check if more than one directory exists, if that is the case,
# we return a non-zero exit code
PIPELINES_RANKED_DIR="$(find "$D3MOUTPUTDIR" -mindepth 2 -path ./temp -prune -o -name "pipelines_ranked" -print)"

if [[  "$(echo "$PIPELINES_RANKED_DIR" | wc -l)" -ne 1 ]]
then
    exit 1
fi

# We check that any pipeline has been exported. This will exit
# the script with non-zero exit code if directory is empty.
find "$PIPELINES_RANKED_DIR" -mindepth 1 | read

OUTPUTDIR="$(dirname "$PIPELINES_RANKED_DIR")"

# Inside TA3-TA2 CI we want to store pipelines as results.
# Results are then stored as CI artifacts.
if [ -e /results ]; then
    cp -a "$PIPELINES_RANKED_DIR" /results/pipelines_ranked

    for dir in pipelines_scored pipelines_searched pipeline_runs additional_inputs; do
        if [ -e "$OUTPUTDIR/$dir" ]; then
            cp -a "$OUTPUTDIR/$dir" "/results/$dir"
        fi
    done
fi
